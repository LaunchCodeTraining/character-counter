# Character Counter

For this exercise you will be writing code that counts the different characters in a given string.

For example given the string "welcome" you should return a HashMap with the following Key Value pairs:

```java
{ 
    "w": 1,
    "e": 2,
    "l": 1,
    "c": 1,
    "o": 1,
    "m": 1
}
```

Some starter code has been provided for you. You can use `javac` and `java` to compile and run your code.

From `character-counter/` you can run `javac org/launchcode/CharacterCounter.java` to compile your code and `java org.launchcode.CharacterCounter` to run your compiled code.

Good luck. Reach out to an instructor if you need help!